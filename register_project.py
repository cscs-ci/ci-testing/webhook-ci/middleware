import asyncio
import os
import random
import secrets
import sqlite3

from server import create_ssh_key, ssh_keyfile

async def main() -> None:
    dbpath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'middleware.db')
    print(f'Using database {dbpath}')
    con = sqlite3.connect(dbpath)
    con.execute('PRAGMA foreign_keys=ON;')

    project_url = input('Project URL: ')
    if project_url[-4:] == '.git':
        project_url = project_url[0:-4]
        print(f'Project-URL ends with ".git", but we want the URL in the DB without .git, hence it was adapted to {project_url}')

    notification_token = input('Notification token - leave empty if you do not want your repository to receive status updates from CI: ')

    private = ''
    while private not in ['y', 'n']:
        private = input('Private repository (y/n): ')

    name = project_url.split('/')[-1]
    name_tmp = input(f'Name [{name}]: ')
    if name_tmp: name = name_tmp

    allow_cray_license = ''
    while allow_cray_license not in ['y', 'n']:
        allow_cray_license = input('Allow cray license to be mounted (y/n): ')

    ci_entrypoint = input('YML entrypoint [.gitlab-ci.yml]: ')
    if not ci_entrypoint: ci_entrypoint = '.gitlab-ci.yml'

    branches = input("Branches to mirror (comma-separated, empty list will only trigger on comment): ")
    branches_list = []
    if branches:
        branches_list = [ branch.strip() for branch in branches.split(',') ]

    whitelisted_users = input("Whitelisted usernames whose PRs are automatically accepted to run CI: ")
    whitelisted_users_list = []
    if whitelisted_users:
        whitelisted_users_list = [ user.strip() for user in whitelisted_users.split(',') ]


    webhook_secret = secrets.token_hex(16)
    mirror_webhook_secret = secrets.token_hex(16)


    project_id = random.randint(1, 2**53)
    while con.execute('select project_id from project where project_id=?', (project_id,)).fetchone() != None:
        project_id = random.randint(1, 2**53)
    repository_id = random.randint(1, 2**53)
    while con.execute('select repository_id from repository where repository_id=?', (repository_id,)).fetchone() != None:
        repository_id = random.randint(1, 2**53)
    pipeline_id = random.randint(1, 2**53)
    while con.execute('select pipeline_id from pipeline where pipeline_id=?', (pipeline_id,)).fetchone() != None:
        pipeline_id = random.randint(1, 2**53)

    con.execute('insert into project (`project_id`,`name`) VALUES (?,?)', (project_id, name))
    con.execute('insert into repository (`repository_id`,`project_id`,`repo_url`,`notification_token`,`webhook_secret`,`private`,`name`) VALUES (?,?,?,?,?,?,?)', (repository_id, project_id, project_url, notification_token, webhook_secret, private=='y', name))
    con.execute('insert into pipeline (`pipeline_id`,`repository_id`,`webhook_secret`,`allow_craylic`,`ci_entrypoint`,`name`) VALUES (?,?,?,?,?,?)', (pipeline_id, repository_id, mirror_webhook_secret, allow_cray_license=='y', ci_entrypoint, 'default'))

    if branches_list:
        con.executemany('insert into branch_default (`repository_id`,`branch`) VALUES (?,?)', zip([repository_id]*len(branches_list), branches_list))

    if whitelisted_users_list:
        con.executemany('insert into git_user_default (`repository_id`, `username`) VALUES (?,?)', zip([repository_id]*len(whitelisted_users_list), whitelisted_users_list))
    con.commit()

    webhook_url = f'https://cicd-ext-mw.cscs.ch/ci/webhook_ci?id={repository_id}'
    if project_url.find('bitbucket.org') != -1:
        webhook_url += f'&secret={webhook_secret}'
        print(f'{project_url}/admin/webhooks')
        print('Title: Arbitrary, e.g. CSCS CI')
        print(f'URL: {webhook_url}')
        print(f'Triggers:\n\tRepository: Push\n\tPull Request: Created, Updated, Comment Created')
    if project_url.find('github.com') != -1:
        print(f'{project_url}/settings/hooks')
        print(f'Webhook-URL: {webhook_url}')
        print('Content-Type: application/json')
        print(f'Secret: {webhook_secret}')
        print(f'Event-Types: Send me everything')

    await create_ssh_key(ssh_keyfile(repository_id, mirror=False, public=False))

    import config
    cfg = config.Config(dbpath)
    print(f'branches to be mirrored={cfg.get_branches_default(repository_id)}')
    print(f'Whitelisted users={cfg.get_git_users_default(repository_id)}')


if __name__ == '__main__':
    asyncio.run(main())
