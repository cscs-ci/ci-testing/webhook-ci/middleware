# python default imports
import asyncio
import datetime
import logging
import os
from typing import Dict

# python 3rtparty imports
import gitlab

# local imports
from server import _do_notify_orig_repo
import config

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

glserver = gitlab.Gitlab.from_config() # type: ignore

project_dir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(project_dir, 'middleware.db')
cfg = config.Config(db_path)

status_by_sha: Dict[str, str] = {} #: Dict[str, str] = dict()

# Gitlab does not always send the correct final status in its pipeline webhooks, such that we receive the status "pending"
# while in real life the pipeline is successful/failed. Therefore we run this script nonstop, which checks every 10 minutes
# all gitlab mirror projects the latest 10 pipelines and updates the status whenever it sees a change from the last seen state
async def main() -> None:
    while True:
        for pipeline_id, target_url, sha, name in cfg.get_status_updates_needed():
            logger.info(f'pipeline_id={pipeline_id} target_url={target_url} sha={sha} name={name}')
            repository_id = cfg.get_repository_id(pipeline_id)
            notification_token = cfg.get_notification_token(repository_id)
            try:
                found_projects = glserver.projects.list(search=pipeline_id, owned=True)
                if len(found_projects) < 1: continue
                mirror_gitlab_repo = found_projects[0]
                pipelines = mirror_gitlab_repo.pipelines.list(sha=sha)
                if len(pipelines)==0: continue
                gitlab_pipeline = pipelines[0]
                url = cfg.get_notification_url(repository_id).replace('{sha}', gitlab_pipeline.sha)
                await _do_notify_orig_repo(url, gitlab_pipeline.status, target_url, name, notification_token)
                if gitlab_pipeline.status in ('success', 'failed', 'canceled', 'skipped'):
                    cfg.del_status_update_needed(pipeline_id, target_url, sha, name)
            except Exception as e:
                logger.warning(f'Exception caught. e={e}')
        await asyncio.sleep(600)
#         last_dt = datetime.datetime.now().timestamp()
#         # we do not cache gitlab's internal project ids, because projects could be deleted at any time
#         for pipeline_id in cfg.get_all_pipelines():
#             logger.info(f'processing pipeline_id={pipeline_id}')
#             repository_id = cfg.get_repository_id(pipeline_id)
#             is_private = cfg.is_private_repo(repository_id)
#             notification_token = cfg.get_notification_token(repository_id)
#             found_projects = glserver.projects.list(search=pipeline_id, owned=True)
#             if len(found_projects) < 1: continue
#             mirror_gitlab_repo = found_projects[0]
#             # we need for each project to keep a set of already processed sha's (there could be the situation that
#             # for the same sha two pipelines were started, however only the latest one should determine the status)
#             # The latest pipeline is the one that is first seen when iterating through the list of gitlab pipelines
#             processed_shas = set()
#             for gitlab_pipeline in mirror_gitlab_repo.pipelines.list(per_page=10, all=False):
#                 id_sha = f'{pipeline_id}_{gitlab_pipeline.sha}'
#                 if status_by_sha.get(id_sha, '') != gitlab_pipeline.status and id_sha not in processed_shas:
#                     try:
#                         logger.info(f'status changed for id_sha={id_sha} from={status_by_sha.get(id_sha, "_unst")} to={gitlab_pipeline.status}')
#                         url = cfg.get_notification_url(repository_id).replace('{sha}', gitlab_pipeline.sha)
#                         target_url = gitlab_pipeline.web_url
#                         if is_private:
#                             target_url = f'https://cicd-ext-mw.cscs.ch/ci/pipeline_results/{pipeline_id}/{mirror_gitlab_repo.id}/{gitlab_pipeline.id}'
#                         await _do_notify_orig_repo(url, gitlab_pipeline.status, target_url, cfg.get_pipeline_name(pipeline_id), notification_token)
#                     except Exception as e:
#                         logger.warning(f'Exception processing id_sha={id_sha}')
#                     status_by_sha[id_sha] = gitlab_pipeline.status
#                 processed_shas.add(id_sha)
#         long_sleep = max(60, 600-(datetime.datetime.now().timestamp()-last_dt))
#         logger.info(f'long sleeping for {long_sleep} seconds')
#         await asyncio.sleep(long_sleep)


if __name__ == '__main__':
    asyncio.run(main())
