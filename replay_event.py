import fileinput
import json
import requests
import sys

# read the first line from stdin and replay it as event - the data should have been logged by server.py and is a JSON string
# with the keys `headers`, `path` and `body`. A request with these headers and body will be resent to path.
# The easiest way is to use grep/head/tail to find the even that you are interested in, and pipe it to this script
# Exactly one event will be replayed, if your grep/head/tail found multiple lines only the very first one will be replayed

linedata = json.loads(fileinput.input().readline())
headers = linedata['headers']
path = linedata['path']
body = linedata['body']

host = headers['Host']
requests.post(f'http://{host}{path}', data=body, headers=headers)
