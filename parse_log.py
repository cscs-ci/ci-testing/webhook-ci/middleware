import json
import sys

for logfile in sys.argv[1:]:
    print(f'Parsing file {logfile}')
    for line in open(logfile):
        jdata = json.loads(line)
        print(f"Event={jdata['headers']['X-GitHub-Event']}")
