from __future__ import annotations

import asyncio
import base64
import contextlib
import copy
import datetime
import enum
import hmac
import json
import logging
import os
import pprint
import re
import shutil
import subprocess
import sys
import time
import traceback
from typing import Any, AsyncIterator, Dict, cast, List, Mapping, Optional, Tuple, TypedDict

from aiohttp import web, ClientSession

import aiohttp_jinja2
import jinja2

import ansi2html # type: ignore

import gitlab

import config

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

routes = web.RouteTableDef()

log_id=0

glserver = gitlab.Gitlab.from_config() # type: ignore
glpath = 'git@gitlab.com:cscs-ci/ci-testing/webhook-ci/mirrors'

project_dir = os.path.dirname(os.path.realpath(__file__))
requests_logpath = os.path.join(project_dir, 'log')

spack_sign_key_path = '/home/anfink/ci_ssh_keys/spack_signing_key.gpg'
s3_creds_path = '/home/anfink/ci_ssh_keys/s3_creds.txt'

db_path = os.path.join(project_dir, 'middleware.db')
cfg = config.Config(db_path)

test_env = len(sys.argv)>1 and sys.argv[1]=='test'
devel_env = len(sys.argv)>1 and sys.argv[1]=='devel'
if test_env or devel_env:
    shutil.copy(db_path, f'/home/tmp/{os.path.basename(db_path)}_{datetime.datetime.now().timestamp()}')

my_ip = '148.187.148.110'
my_ip = 'cicd-ext-mw.cscs.ch'
port = 8357
if test_env: port = 8358
if devel_env: port = 8359

url_prefix = 'ci'
if test_env: url_prefix = 'citest'
elif devel_env: url_prefix = 'cidevel'
base_url = f'https://{my_ip}/{url_prefix}'
webhook_pipeline = f'{base_url}/webhook_gitlab_pipeline'

error_ci_run_no_pipeline_name = f'{base_url}/ci_run_no_pipeline_name'

locked_repo_dirs = set()

@contextlib.asynccontextmanager
async def lock_repo_dir(repo_dir: str) -> AsyncIterator[str]:
    try:
        while repo_dir in locked_repo_dirs:
            logger.info(f'repo_dir={repo_dir} is currently locked. Waiting 1s')
            await asyncio.sleep(1)
        logger.info(f'Locking repo_dir={repo_dir}')
        locked_repo_dirs.add(repo_dir)
        yield repo_dir
    finally:
        logger.info(f'Unlocking repo_dir={repo_dir}')
        if repo_dir in locked_repo_dirs: locked_repo_dirs.remove(repo_dir)


async def log_request(req: web.Request) -> None:
    global log_id
    this_log_id = log_id
    log_id += 1
#    logger.info(f'log_id={this_log_id}: path_qs={req.path_qs} headers={req.headers}')
    body = (await req.read()).decode('utf-8')
    open(requests_logpath, 'a').write(json.dumps({'timestamp': datetime.datetime.now().timestamp(), "headers": {k: v for k,v in req.headers.items()}, "path": req.path_qs, "body": body})+'\n')
#    logger.info(f'log_id={this_log_id}: body={body}')

def signature_match(body: bytes, signature: str, secret: str) -> bool:
    # secret is a str. We allow only ascii characters here (we generate it, i.e. we have full control that this will be always true)
    # if it is not ascii, then secret.encode fails, i.e. the error will be visible directly
    cmpsig = 'sha256=' + hmac.new(secret.encode('ascii'), body, digestmod='sha256').hexdigest()
    match = hmac.compare_digest(signature, cmpsig)
    if not match:
        logger.error(f"signature of the request was not matched. Expected signature={signature}, got signature={cmpsig}")
    return match

async def get_mirror_repo(pipeline_id: int) -> Optional[gitlab.v4.objects.projects.Project]:
    found_projects = glserver.projects.list(search=pipeline_id, owned=True)
    assert(len(found_projects) <= 1)
    return found_projects[0] if len(found_projects)==1 else None

class RepositoryEvent(object):
    class Type(enum.Enum):
        github = 1
        gitlab = 2
        bitbucket = 3
        unknown = 99

    def __init__(self) -> None:
        self._repository_id = 0
        # pipeline_id will be only set when this is a Gitlab-Mirror-Webhook call
        self._pipeline_id = 0
        self._name = ''
        self._url = ''
        self._git_url = ''
        self._reqdataraw = b''
        self._reqdata: Dict[str, Any] = dict()
        self._reqheaders: Dict[str, str] = dict()
        self._notification_token = ''
        self._gitlab_notification_token = ''
        self._private = False
        self._type = RepositoryEvent.Type.unknown

    @staticmethod
    async def from_request(req: web.Request) -> RepositoryEvent:
        ret = RepositoryEvent()
        ret._reqdataraw = await req.read()
        ret._reqdata = json.loads(ret._reqdataraw)
        ret._reqheaders = {k: v for k,v in req.headers.items()}
        if req.path == '/webhook_gitlab_pipeline':
            # mirror webhook --> remember repositry and pipeline ids
            ret._repository_id = int(req.query['repository_id'])
            ret._pipeline_id = int(req.query['pipeline_id'])
        else:
            ret._repository_id = int(req.query['id'])
        ret._private = cfg.is_private_repo(ret.repository_id)
        if 'X-GitHub-Event' in ret._reqheaders:
            ret._name = ret._reqdata['repository']['name']
            ret._url = ret._reqdata['repository']['html_url']
            ret._git_url = ret._reqdata['repository']['ssh_url'] if ret._private else ret._reqdata['repository']['clone_url']
            assert ret._url == cfg.get_repo_url(ret.repository_id), 'The URL does not match the registered URL'
            ret._type = RepositoryEvent.Type.github
        elif 'X-Gitlab-Event' in ret._reqheaders:
            # Gitlab can be an original gitlab repository or a mirror repository
            ret._name = ret._reqdata['project']['name']
            ret._url = ret._reqdata['project']['web_url']
            ret._git_url = ret._reqdata['project']['git_ssh_url'] if ret._private else ret._reqdata['project']['git_http_url']
            if req.path == '/webhook_gitlab_pipeline':
                assert ret._url == cfg.get_mirror_url(ret.pipeline_id), 'The event URL does not match the registered mirror URL'
            else:
                assert ret._url == cfg.get_repo_url(ret.repository_id), 'The event URL does not match the registered repository URL'
            ret._type = RepositoryEvent.Type.gitlab
        elif 'X-Event-Key' in ret._reqheaders and ret._reqheaders.get('User-Agent', '').find('Bitbucket') != -1:
            ret._name = ret._reqdata['repository']['name']
            ret._url = ret._reqdata['repository']['links']['html']['href']
            ret._git_url = ret._url.replace('https://bitbucket.org/', 'git@bitbucket.org:', 1) if ret._private else ret._url
            assert ret._url == cfg.get_repo_url(ret.repository_id), 'The URL does not match the registered gitlab URL'
            ret._type = RepositoryEvent.Type.bitbucket
        ret._notification_token = cfg.get_notification_token(ret.repository_id)

        return ret

    @property
    def repository_id(self) -> int:
        return self._repository_id
    @property
    def pipeline_id(self) -> int:
        return self._pipeline_id
    @pipeline_id.setter
    def pipeline_id(self, pipeline_id: int) -> None:
        self._pipeline_id = pipeline_id
    @property
    def name(self) -> str:
        return self._name
    @property
    def url(self) -> str:
        return self._url
    @property
    def git_url(self) -> str:
        return self._git_url
    @property
    def event_body_raw(self) -> bytes:
        return self._reqdataraw
    @property
    def event_body(self) -> Dict[str, Any]:
        return self._reqdata
    @property
    def event_headers(self) -> Dict[str, str]:
        return self._reqheaders
    @property
    def notification_token(self) -> str:
        return self._notification_token
    @property
    def private(self) -> bool:
        return self._private
    @property
    def type(self) -> RepositoryEvent.Type:
        return self. _type

    @property
    def clone_path(self) -> str:
        return os.path.join('/home/tmp', f'{cfg.get_repository_name(self.repository_id)}-{self.repository_id}')

    async def get_mirror_repo(self) -> Optional[gitlab.v4.objects.projects.Project]:
        return await get_mirror_repo(self.pipeline_id)

    def push_event_branches(self) -> List[str]:
        if self.type in [RepositoryEvent.Type.gitlab, RepositoryEvent.Type.github] and 'ref' in self.event_body:
            return [self.event_body['ref'].replace('refs/heads/', '')]
        elif self.type == RepositoryEvent.Type.bitbucket and 'push' in self.event_body:
            ret = []
            for c in self.event_body['push']['changes']:
                if c['new'] and c['new']['type'] == 'branch':
                    ret.append(c['new']['name'])
            return ret
        return []


# helper function to run an external process, wait for it and check error code to be 0 (assertion exception otherwise)
# WARNING: do not set get_stdout=True, when you expect an "unlimited" amount of data. All data is buffered in memory!
async def run_proc(prog: str, *args: str, allow_fail: bool=False, get_stdout: bool=False, **kwargs: Any) -> str:
    logger.info(f'running command: {" ".join([prog, *args])} in directory={kwargs.get("cwd", None)}')
    if get_stdout:
        kwargs['stdout'] = subprocess.PIPE
        kwargs['stderr'] = subprocess.STDOUT
    proc = await asyncio.create_subprocess_exec(prog, *args, **kwargs)
    stdout = ''
    if get_stdout: stdout = (await proc.communicate())[0].decode('utf-8')
    proc_retcode = await proc.wait()
    if not allow_fail:
        assert(proc_retcode == 0)
    return stdout


def get_branches_to_mirror(repo: RepositoryEvent) -> List[str]:
    return cfg.get_branches_pipeline(repo.pipeline_id) or cfg.get_branches_default(repo.repository_id)

def get_git_users(repo: RepositoryEvent) -> List[str]:
    return cfg.get_git_users_pipeline(repo.pipeline_id) or cfg.get_git_users_default(repo.repository_id)

def ssh_keydir(mirror: bool) -> str:
    return os.path.join(os.environ['HOME'], 'ci_ssh_keys', 'mirrors' if mirror else 'orig')
def ssh_keyfile(repo_or_pipeline_id: int, mirror: bool, public: bool) -> str:
    return os.path.join(ssh_keydir(mirror), f'key_{repo_or_pipeline_id}'+('.pub' if public else ''))

async def create_ssh_key(keypath: str) -> str:
    if not os.path.exists(keypath):
        await run_proc('ssh-keygen', '-f', keypath, '-t', 'ed25519', '-C', 'CSCS-CI', '-P', '')
    else:
        logger.warning(f'SSH keyfile for keypath={keypath} exists already. Skipping to create a new keyfile')
    return open(keypath+'.pub').read().strip()


async def get_pr_sha(repo: RepositoryEvent, pr_id: int) -> str:
    stdout = await run_proc('git', 'ls-remote', repo.git_url, f'refs/pull/{pr_id}/head', get_stdout=True, env=git_env(repo.repository_id, mirror=False))
    return stdout.split()[0]


async def init_pipeline_repo(repo: RepositoryEvent) -> None:
    mirror_repo_name = f'{cfg.get_repository_name(repo.repository_id)}-{cfg.get_pipeline_name(repo.pipeline_id)}'
    if await repo.get_mirror_repo() == None:
        # repository URL will be: https://gitlab.com/cscs-ci/ci-testing/webhook-ci/mirrors/REPOSITORY_ID/PIPELINE_ID
        all_mirrors = list(glserver.groups.list(iterator=True, search='webhook-ci/mirrors'))
        mirror_group = [ x for x in all_mirrors if x.path=='mirrors'][0]
        repo_groups = [x for x in all_mirrors if x.path==str(repo.repository_id)]
        repo_group = (repo_groups[0] if len(repo_groups)==1
                else glserver.groups.create({'name': cfg.get_repository_name(repo.repository_id),
                    'path': str(repo.repository_id),
                    'parent_id': mirror_group.id,
                    'emails_disabled': True,
                    'visibility': 'private' if repo.private else 'public'}))
        logger.info(f'Mirror repository "{mirror_repo_name} not found. Creating and importing project to gitlab')
        # update in the database the mirror-url
        new_project = glserver.projects.create(dict(name=mirror_repo_name,
                path=str(repo.pipeline_id),
                description=f'Mirror of {repo.url}',
                namespace_id=repo_group.id,
                public_builds=True, # this does not allow public access to the build logs for private projects though (True by default anyway, just here for reference)
                initialize_with_readme=True, # initialize with a readme, which creates a default branch (and a branch protection) such that we can delete the branch protection directly
                visibility='private' if repo.private else 'public',
                emails_disabled=True,
                ci_config_path=cfg.get_ci_entrypoint(repo.pipeline_id)))
        mirror_url = new_project.web_url
        logger.info(f'Updating mirror_url in the database to new value: {mirror_url}')
        cfg.update_mirror_url(repo.pipeline_id, mirror_url)
        new_project.hooks.create({'url': f'{webhook_pipeline}?repository_id={repo.repository_id}&pipeline_id={repo.pipeline_id}',
            'pipeline_events': 1,
            'push_events': 0,
            'token': get_mirror_webhook_secret(repo.pipeline_id)})
        new_project.variables.create({'key': 'CSCS_REGISTRY_PATH', 'value': f'jfrog.svc.cscs.ch/contbuild/testing/anfink/{repo.repository_id}'})
        new_project.variables.create({'key': 'CSCS_REGISTRY_LOGIN', 'value': 'YES'})
        new_project.variables.create({'key': 'CSCS_SKIP_GIT_CLONE_SARUS_RUNNER', 'value': 'YES'})

        # SSH keyfiles
        ssh_key = await create_ssh_key(ssh_keyfile(repo.pipeline_id, mirror=True, public=False))
        new_project.keys.create({'title': 'CSCS-CI', 'key': ssh_key, 'can_push': True})

        # delete branch protection - this is a pure mirror, no branch protection is wanted!
        for b in new_project.protectedbranches.list(iterator=True):
            b.delete()
        await asyncio.sleep(3) # we need to sleep a little bit (gitlab does not unprotect the branch instantly)
    else:
        logger.info(f'Mirror repo {mirror_repo_name} exists already. init_pipeline_repo is a no-op.')


def git_env(repo_or_pipeline_id: int, mirror: bool) -> Mapping[str, str]:
    new_env = copy.deepcopy(os.environ)
    new_env['GIT_SSH_COMMAND'] = f'ssh -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -i {ssh_keyfile(repo_or_pipeline_id, mirror=mirror, public=False)}'
    return new_env


async def _clone_or_update(repo: RepositoryEvent) -> None:
    repo_path = repo.clone_path
    if not os.path.exists(repo_path):
        # clone with `--bare` instead of `--mirror`. The former does not push refs/pull/*, also Git LFS poses problems when we clone with `--mirror`
        # and try to push with `--mirror`
        await run_proc('git', 'clone', '--bare', repo.git_url, repo_path, env=git_env(repo.repository_id, mirror=False))
    #    await run_proc('git', 'clone', '--mirror', repo_url, repo_path)
    else:
        await run_proc('git', 'fetch', '--prune', '--force', repo.git_url, '+refs/heads/*:refs/heads/*',
            cwd=repo_path, env=git_env(repo.repository_id, mirror=False))


async def update_repo(repo: RepositoryEvent) -> None:
    repo_path = repo.clone_path
    async with lock_repo_dir(repo_path):
        await _clone_or_update(repo)
        for pipeline_id in cfg.get_pipelines(repo.repository_id):
            try:
                repo.pipeline_id = pipeline_id
                await init_pipeline_repo(repo)
                branches = get_branches_to_mirror(repo)
                branches_to_push = set(branches).intersection(repo.push_event_branches())
                if branches_to_push:
                    try:
                        await run_proc('git', 'push', '--force', '--prune', '-o', 'ci.skip', f'{glpath}/{repo.repository_id}/{repo.pipeline_id}',
                                *branches_to_push, cwd=repo_path, env=git_env(repo.pipeline_id, mirror=True))
                        mirror_repo = await get_mirror_repo(pipeline_id)
                        if mirror_repo is not None:
                            for branch in branches_to_push:
                                mirror_repo.pipelines.create({'ref':branch, 'variables': ci_extra_vars(repo)})
                    except Exception as e:
                        logger.error(f'There was an error trying to push the branches [{branches_to_push}] for repo with repository_id={repo.repository_id} and pipeline_id={pipeline_id}. Exception={e}\n{traceback.format_exc()}')
            except Exception as e:
                logger.error(f'There was an error trying to update the repo with repository_id={repo.repository_id} and pipeline_id={pipeline_id}. Exception={e}\n{traceback.format_exc()}')


def ci_extra_vars(repo: RepositoryEvent) -> List[Dict[str, str]]:
    privkey = open(ssh_keyfile(repo.repository_id, mirror=False, public=False)).read()
    spack_sign_key = open(spack_sign_key_path).read()
    s3_creds = open(s3_creds_path).read().splitlines(keepends=False)
    return [
            {'key': 'CSCS_CI_MW_URL', 'value': base_url},
        ]

async def push_branch_for_pr(repo: RepositoryEvent, pr_id: int, cscs_run_comment: bool=False) -> None:
    repo_path = repo.clone_path
    async with lock_repo_dir(repo_path):
        await _clone_or_update(repo)
        all_pipeline_ids = cfg.get_pipelines(repo.repository_id)

        # notify error status, when comment does not specify pipelines that should run and there is more than one pipeline defined
        if cscs_run_comment and repo.event_body['comment']['body'] == 'cscs-ci run' and len(all_pipeline_ids)>1:
            await notify_ci_error(repo, error_ci_run_no_pipeline_name, await get_pr_sha(repo, pr_id))
            return
        elif cscs_run_comment:
            await notify_ci_error_clear(repo, await get_pr_sha(repo, pr_id))

        for pipeline_id in all_pipeline_ids:
            repo.pipeline_id = pipeline_id
            await init_pipeline_repo(repo)
            push_to_gitlab = False
            # first case is due to a PR event, i.e. we have a pull_request event body
            if cscs_run_comment==False:
                if cfg.get_trigger_pr(pipeline_id) == False:
                    logger.info(f'Pipeline with name={cfg.get_pipeline_name(pipeline_id)} is not triggered automatically because trigger_pr is set to false')
                    continue
                branches = get_branches_to_mirror(repo)
                # check if the branch into which we would like to merge this PR is one of the branches that needs testing
                # ignore PR's if branches is an empty list - if all branches are tested, then this PR is already tested, unless
                # the PR comes in from a forked repo. PR's from a forked repo must be thought about - is it safe to let them run?
                if branches and repo.event_body['pull_request']['base']['ref'] in branches:
                    # check if this pull requests head is coming from a forked repo and if yes then continue only for whitelisted users
                    if repo.event_body['pull_request']['head']['repo']['id'] != repo.event_body['repository']['id']:
                        pr_user =  repo.event_body['pull_request']['head']['repo']['owner']['login']
                        if pr_user not in get_git_users(repo):
                            logger.info(f'Users={pr_user} is not allowed for automatic CI. This PR will not be mirrored.')
                            return
                    push_to_gitlab = True
                    logger.info(f'PR with ID={pr_id} targets branch {repo.event_body["pull_request"]["base"]["ref"]} which is in the whitelisted CI branches')
            # second case is due to a PR comment, i.e. we have an issue_comment event body
            else:
                comment_user = repo.event_body['comment']['user']['login']
                if not repo.event_body['comment']['body'].startswith('cscs-ci run'):
                    logger.info(f'Comment body does NOT start with `cscs-ci run`. This PR will not be mirrored')
                    return
                if comment_user not in get_git_users(repo):
                    logger.info(f'Users={comment_user} is not allowed to comment `cscs-ci run`. This PR will not be mirrored.')
                    return
                # pipeline names separated by comma or whitespace (or only one pipeline is available and no pipeline name was specified)
                if (len(all_pipeline_ids)==1 and repo.event_body['comment']['body']) or re.search(f'[ ,]{cfg.get_pipeline_name(pipeline_id)}([ ,]|$)', repo.event_body['comment']['body']):
                    push_to_gitlab = True

            if push_to_gitlab:
                # create a branch named __CSCSCI__pr{pr_id} and push it to gitlab
                branchname = f'__CSCSCI__pr{pr_id}'
                await run_proc('git', 'fetch', repo.git_url, f'refs/pull/{pr_id}/head:{branchname}',
                        cwd=repo_path, env=git_env(repo.repository_id, mirror=False))
                # skip automatic CI, rather enforce a CI pipeline (can happen when no additional commit was done, but the  comment cscs-ci run was commented a second time)
                # https://docs.gitlab.com/ee/user/project/push_options.html#push-options-for-gitlab-cicd
                await run_proc('git', 'push', '--force', '--prune', '-o', 'ci.skip', f'{glpath}/{repo.repository_id}/{repo.pipeline_id}', branchname,
                        cwd=repo_path, env=git_env(repo.pipeline_id, mirror=True))
                mirror_repo = await get_mirror_repo(repo.pipeline_id)
                if mirror_repo is not None:
                    try:
                        mirror_repo.pipelines.create({'ref':branchname, 'variables': ci_extra_vars(repo)})
                    except:
                        # sometimes Gitlab fails with a "No such branch" error, hence we wait 20 seconds and retry triggering the pipeline
                        time.sleep(20)
                        mirror_repo.pipelines.create({'ref':branchname, 'variables': ci_extra_vars(repo)})
                # deleting the branch in the cloned repo has actually no real effect, because we never push all refspecs to the mirror, i.e. it will stay there
                await run_proc('git', 'branch', '-D', branchname, cwd=repo_path)



def get_id_for_request(request: web.Request) -> int:
    return int(request.query['id'])

def get_webhook_secret(repository_id: int) -> str:
    return cfg.get_webhook_secret(repository_id)
def get_mirror_webhook_secret(pipeline_id: int) -> str:
    return cfg.get_mirror_webhook_secret(pipeline_id)


async def _do_notify_orig_repo(url: str, status: str, target_url: str, pipeline_name: str, token: str, description: str='') -> str:
    status_name = f'cscs/{pipeline_name}'
    if url.startswith('https://api.bitbucket.org'):
        if status == 'running' or status == 'created' or status == 'pending': status = 'INPROGRESS'
        elif status == 'success': status = 'SUCCESSFUL'
        elif status == 'failed': status = 'FAILED'
        elif status == 'canceled': status = 'STOPPED'
        elif status == 'skipped': return 'skipped'
        else:
            logger.warning(f'received status from mirror repository that cannot be translated to bitbucket. Status={status}')
            return 'Nothing to notify'
        headers = {
                'Authorization': f'Basic {base64.b64encode(token.encode("utf-8")).decode("utf-8")}'
            }
        data = {
                'state': status,
                'url': target_url,
                'description': description,
                'key': status_name
            }

    elif url.startswith('https://api.github.com'):
        if status == 'running' or status == 'created':   status = 'pending'
        if status == 'failed':    status = 'failure'
        if status == 'canceled':  status = 'error'
        if status == 'skipped': return 'skipped'
        headers = {
                'Authorization': f'Bearer {token}'
            }
        data = {
                'state': status,
                'target_url': target_url,
                'description': description,
                'context': status_name
            }

    logger.info(f'Notifying original repository for status. url={url} status={status}')
    async with ClientSession() as session:
        async with session.post(url, headers=headers, json=data) as r:
            r.raise_for_status()
            return await r.text()

async def notify_ci_error(repo: RepositoryEvent, error_page: str, sha: str) -> str:
    target_url = error_page
    url = cfg.get_notification_url(repo.repository_id).replace('{sha}', sha)
    status = 'failed'
    pipeline_name = 'ci_error'
    return await _do_notify_orig_repo(url, status, target_url, pipeline_name, repo.notification_token)

async def has_ci_error(repo: RepositoryEvent, sha: str) -> bool:
    url = cfg.get_notification_url(repo.repository_id).replace('{sha}', sha)
    token = repo.notification_token
    try:
        if url.startswith('https://api.bitbucket.org'):
            headers = {
                    'Authorization': f'Basic {base64.b64encode(token.encode("utf-8")).decode("utf-8")}'
                }
            url += '/build/cscs/ci_error'
        elif url.startswith('https://api.github.com'):
            headers = {
                    'Authorization': f'Bearer {token}'
                }
            url += '?per_page=100'

        data = {}
        async with ClientSession() as session:
            async with session.get(url, headers=headers) as r:
                r.raise_for_status()
                data = json.loads(await r.text())
        if url.startswith('https://api.bitbucket.org'):
            return bool(data.get('key', '')=='cscs/ci_error' and data.get('state', '')=='FAILED')
        elif url.startswith('https://api.github.com'):
            return len([x for x in data if x.get('context','')=='cscs/ci_error'])>0
    except Exception as e:
        logger.error(f'Caught exception e={e}.\n{traceback.format_exc()}')
    return False


async def notify_ci_error_clear(repo: RepositoryEvent, sha: str) -> str:
    if await has_ci_error(repo, sha):
        url = cfg.get_notification_url(repo.repository_id).replace('{sha}', sha)
        return await _do_notify_orig_repo(url, 'success', '', 'ci_error', repo.notification_token, 'No more errors')
    return ''


async def notify_orig_repo(repo: RepositoryEvent) -> str:
#    target_url = f'{repo.event_body["project"]["web_url"]}/-/pipelines/{repo.event_body["object_attributes"]["id"]}'
    target_url = f'{base_url}/pipeline/results/{repo.pipeline_id}/{repo.event_body["project"]["id"]}/{repo.event_body["object_attributes"]["id"]}'
    if not repo.private: target_url += '?type=gitlab'
    url = cfg.get_notification_url(repo.repository_id).replace('{sha}', repo.event_body['object_attributes']['sha'])
    status = repo.event_body['object_attributes']['status']
    pipeline_name = cfg.get_pipeline_name(repo.pipeline_id)

    other_status = glserver.projects.get(repo.event_body['project']['id']).pipelines.get(repo.event_body['object_attributes']['id']).status
    if status != other_status and other_status in ('success', 'failed'):
        logger.error(f'ERROR condition in notify_orig_repo. The received webhook claims status={status} but getting the status via API claims the status={other_status}')

    if status in ('success', 'failed', 'skipped', 'canceled'):
        cfg.del_status_update_needed(repo.pipeline_id, target_url, repo.event_body['object_attributes']['sha'], pipeline_name)
    else:
        cfg.add_status_update_needed(repo.pipeline_id, target_url, repo.event_body['object_attributes']['sha'], pipeline_name)

    return await _do_notify_orig_repo(url, status, target_url, pipeline_name, repo.notification_token)



@routes.post('/webhook_gitlab_pipeline')
async def handle_gitlab_pipeline(request: web.Request) -> web.Response:
    await log_request(request)
    repo = await RepositoryEvent.from_request(request)
    assert repo.event_headers['X-Gitlab-Token']==get_mirror_webhook_secret(repo.pipeline_id), "Gitlab notification token mismatch"
    text = 'ok'
    if repo.event_headers['X-Gitlab-Event']=='Pipeline Hook' and repo.event_body['object_attributes']['source'] != 'parent_pipeline':
        text = await notify_orig_repo(repo)
    return web.Response(text=text)


@routes.post('/webhook_ci')
async def handle_webhook_ci(request: web.Request) -> web.Response:
    await log_request(request)
    secret = get_webhook_secret(get_id_for_request(request))
    repo = await RepositoryEvent.from_request(request)
    global statuses_git_url

    # NOTE: If a repository needs to be updated (and potentially be init'd), we shield from cancellation. Github has for instance
    #       a very short timeout. This way the the update_repo can finish, however anything else after `update_repo` will not be
    #       executed
    if 'X-GitHub-Event' in request.headers:
        if not signature_match(repo.event_body_raw, repo.event_headers.get('X-Hub-Signature-256', ''), secret=secret):
            return web.Response(status=403, text='signature mismatch')
        ev_type = request.headers['X-GitHub-Event']
        cfg.update_notification_url(repo.repository_id, repo.event_body['repository']['statuses_url'])
        logger.info(f'Remembering for project with id={repo.repository_id} the statuses_url={cfg.get_notification_url(repo.repository_id)}')
        if ev_type == 'ping':
            logger.info(f'Received a ping event from Github. Initializing repository for repo={repo.name}')
            await asyncio.shield(update_repo(repo))
        elif ev_type == 'push':
            logger.info(f'Received a push event from Github. Updating repository for repo={repo.name}')
            await asyncio.shield(update_repo(repo))
        elif ev_type == 'pull_request':
            # check if the PR head has changed and continue only when the PR state is open (closed PR's will not be tested!)
            logger.info(f'Received a pull_request event from Github. action={repo.event_body["action"]} state={repo.event_body["pull_request"]["state"]}')
            if repo.event_body['action'] in ('opened', 'synchronize') and repo.event_body['pull_request']['state'] == 'open':
                logger.info(f'pushing a branch for PR={repo.event_body["number"]}')
                await asyncio.shield(push_branch_for_pr(repo, repo.event_body['number']))
        elif ev_type == 'issue_comment':
            # check if the comment is the text `cscs-ci run` and run CI if the comment is by a whitelisted user
            logger.info(f'Received an issue comment from github action={repo.event_body["action"]} and body={repo.event_body["comment"]["body"]}')
            if repo.event_body['action'] in ('created', 'edited') and repo.event_body['comment']['body'].startswith('cscs-ci run') and 'pull_request' in repo.event_body['issue']:
                logger.info(f'cscs-ci run comment: Pushing a branch for PR={repo.event_body["issue"]["number"]}')
                await asyncio.shield(push_branch_for_pr(repo, repo.event_body['issue']['number'], cscs_run_comment=True))
        else:
            logger.info(f'Github-Event={ev_type} is not being processed any further')
        text = 'ok'
    elif 'X-Event-Key' in request.headers and request.headers.get('User-Agent', '').find('Bitbucket') != -1:
        # bitbucket repository
        if 'secret' not in request.query or secret != request.query['secret']:
            return web.Response(status=403, text='secret mismatch')
        ev_type = request.headers['X-Event-Key']
        cfg.update_notification_url(repo.repository_id, f'{repo.event_body["repository"]["links"]["self"]["href"]}/commit/{{sha}}/statuses/build')
        logger.info(f'Remembering for project with id={repo.repository_id} the statuses_url={cfg.get_notification_url(repo.repository_id)}')
        if ev_type == 'repo:push':
            logger.info(f'Received a push event from Bitbucket. Updating repository for repo={repo.name}')
            await asyncio.shield(update_repo(repo))
        else:
            logger.info(f'Bitbucket-event={ev_type} is not beig processed any further')
        text = 'ok'
    else:
        text = 'git instance not supported'
    return web.Response(text=text)


def get_class_for_job_status(job_status: str) -> str:
    if job_status == 'success': return 'success'
    if job_status == 'pending': return 'warning'
    if job_status == 'failed': return 'danger'
    if job_status == 'running': return 'primary'
    if job_status == 'canceled': return 'dark'
    return 'secondary'


def combine_status(current: str, other: str) -> str:
    if current == 'canceled' or other == 'canceled': return 'canceled'
    if current == 'failed' or other == 'failed': return 'failed'
    if current == 'pending' or other == 'pending': return 'pending'
    if current == 'success' and other == 'success': return 'success'
    return other

def icon_for_status(status: str) -> str:
    if status == 'success': return 'check-circle'
    if status == 'pending': return 'pause-circle'
    if status == 'failed': return 'x-circle'
    if status == 'running': return 'hourglass-split'
    if status == 'canceled': return 'slash-circle'
    return 'patch-question'


@routes.get('/pipeline_results/{pipeline_id}/{gitlab_project_id}/{gitlab_pipeline_id}')
async def handle_pipeline_results(request:web.Request) -> web.Response:
    await log_request(request)
    pipeline_id = int(request.match_info['pipeline_id'])
    gitlab_project_id = request.match_info['gitlab_project_id']
    gitlab_pipeline_id = request.match_info['gitlab_pipeline_id']

    pipeline = glserver.projects.get(gitlab_project_id).pipelines.get(gitlab_pipeline_id)

    # security check if the gitlab_project_id matches with the data saved with project_id
    if not pipeline.web_url.startswith(cfg.get_mirror_url(pipeline_id)):
        return web.Response(text="Mismatching gitlab_url in database and requested pipeline")

    # forward to gitlab if type==gitlab
    if request.query.get('type', '') == 'gitlab': raise web.HTTPFound(location=pipeline.web_url)

    repository_id = cfg.get_repository_id(pipeline_id)
    repo_url = cfg.get_repo_url(repository_id)
    repo_type = RepositoryEvent.Type.unknown
    if repo_url.startswith('https://github.com'): repo_type = RepositoryEvent.Type.github
    elif repo_url.startswith('https://gitlab.com'): repo_type = RepositoryEvent.Type.gitlab
    elif repo_url.startswith('https://bitbucket.org'): repo_type = RepositoryEvent.Type.bitbucket

    authorized = False
    if check_authorized(request) == True and parse_authorization(request)[0] == str(repository_id):
        authorized = True

    pr_id = ''
    ref = pipeline.ref
    if ref.startswith('__CSCSCI__pr'):
        pr_id = pipeline.ref.replace('__CSCSCI__pr', '')
        ref = f'PR {pr_id}'
    pr_url = ''
    if pr_id:
        if repo_type==RepositoryEvent.Type.github:
            pr_url = f'{repo_url}/pull/{pr_id}'
        # elif: TODO gitlab / bitbucket
    else:
        if repo_type==RepositoryEvent.Type.github:
            pr_url = f'{repo_url}/tree/{ref}'

    commit_url = ''
    if repo_url.startswith('https://github.com'):
        if pr_id:
            commit_url = f'{repo_url}/pull/{pr_id}/commits/{pipeline.sha}'
        else:
            commit_url = f'{repo_url}/commit/{pipeline.sha}'
    # elif: TODO: gitlab / bitbucket

    title = f'{cfg.get_repository_name(repository_id)} - {cfg.get_pipeline_name(pipeline_id)}'

    favicon = dict(
        success='check-circle-green.svg',
        failed='x-circle-red.svg',
        running='hourglass-split-blue.svg',
        pending='pause-circle-yellow.svg',
        canceled='slash-circle-black.svg'
    ).get(pipeline.status, '')

    all_jobs = list(pipeline.jobs.list(iterator=True))
    class _Job(TypedDict):
        status: str
        href: str
        text_color: str
        name: str
        icon: str
        duration: str
    class _Stage(TypedDict):
        name: str
        text_color: str
        icon: str
        jobs: List[_Job]
    stages: List[_Stage] = []
    stage_jobs: List[_Job] = []
    stage_name = ''
    stage_status = 'success'
    for job in reversed(all_jobs):
        if stage_name != job.stage:
            if stages:
                stages[-1]['name'] = stage_name
                stages[-1]['icon'] = icon_for_status(stage_status)
                stages[-1]['text_color'] = f'text-{get_class_for_job_status(stage_status)}'
            stages.append(_Stage(name=stage_name, text_color='', icon='', jobs=stage_jobs))
            stage_jobs = []
            stage_name = job.stage
            stage_status = 'success'
        stage_status = combine_status(stage_status, job.status)
        stages[-1]['jobs'].append(_Job(status=job.status,
                                       href=f'/{url_prefix}/job_results/{pipeline_id}/{gitlab_project_id}/{job.id}',
                                       text_color=f'text-{get_class_for_job_status(job.status)}',
                                       name=job.name,
                                       icon=icon_for_status(job.status),
                                       duration=f'{int(job.duration)} sec' if job.duration else ''))
    if stages:
        stages[-1]['name'] = stage_name
        stages[-1]['icon'] = icon_for_status(stage_status)
        stages[-1]['text_color'] = f'text-{get_class_for_job_status(stage_status)}'

    context = dict(
        ref              = ref,
        title            = title,
        pr_url           = pr_url,
        commit_url       = commit_url,
        favicon          = favicon,
        url_prefix       = request.app['url_prefix'],
        pipeline         = pipeline,
        text_color       = f'text-{get_class_for_job_status(pipeline.status)}',
        icon             = icon_for_status(pipeline.status),
        stages           = stages,
        authorized       = authorized
    )
    response = aiohttp_jinja2.render_template("pipeline_status.html", request, context=context)
    return response


@routes.get('/job_results/{pipeline_id}/{gitlab_project_id}/{job_id}')
async def handle_job_results(request:web.Request) -> web.Response:
    # TODO: security check if access should be granted
    await log_request(request)
    pipeline_id = request.match_info['pipeline_id']
    gitlab_project_id = request.match_info['gitlab_project_id']
    job_id = request.match_info['job_id']
    job = glserver.projects.get(gitlab_project_id).jobs.get(job_id)
    job_trace = job.trace()
    html = ansi2html.Ansi2HTMLConverter(scheme='xterm', title=f'Results for job {job.name}').convert(job_trace.decode('utf-8'))
    # add a link back the pipeline status page
    pipeline_link = f'/{url_prefix}/pipeline_results/{pipeline_id}/{gitlab_project_id}/{job.pipeline["id"]}'
    html = html.replace('</body>', f'<a href="{pipeline_link}" style="position:fixed; top:10px; right: 10px; color:white">Back to overview</a></body>')
    return web.Response(text=html, content_type='text/html')


@routes.get('/static/{path:.+}')
async def handle_static_files(request: web.Request) -> web.FileResponse:
    await log_request(request)
    path = request.match_info['path']
    return web.FileResponse(os.path.join(project_dir, 'static', path))


def parse_authorization(request: web.Request) -> Tuple[str, str]:
    if 'Authorization' in request.headers and request.headers['Authorization'].split(" ")[0].lower() == 'basic':

        username, password = base64.b64decode(request.headers['Authorization'].split(' ')[1]).decode('utf-8').split(':')
        return (username, password)
    return ('', '')

def check_authorized(request: web.Request) -> bool:
    try:
        username, password = parse_authorization(request)
        repo_id_int = int(username)
        if cfg.get_webhook_secret(repo_id_int) == password:
            return True
        return False
    except:
        return False

ignore_logged_in_user = set()
@routes.get('/setup_ci_relogin')
async def handle_setup_ci_relogin(request: web.Request) -> web.Response:
    location = f'/{url_prefix}{request.app.router["setup_ci"].url_for()}'
    username, password = parse_authorization(request)
    if username: ignore_logged_in_user.add(username)
    raise web.HTTPFound(location=location)

@routes.get('/setup_ci', name='setup_ci')
async def handle_setup_ci(request: web.Request) -> web.Response:
    await log_request(request)
    if check_authorized(request) == False:
        return web.Response(status=401, headers={'WWW-Authenticate': 'Basic realm="Access to CI setup page"'})
    username = parse_authorization(request)[0]
    if username in ignore_logged_in_user:
        ignore_logged_in_user.remove(username)
        return web.Response(status=401, headers={'WWW-Authenticate': 'Basic realm="Access to CI setup page"'})

    # we are authorized
    repo_id, webhook_secret = parse_authorization(request)
    repo_id_int = int(repo_id)

    selected_pipeline = request.query.get('pipeline', -1)

    pipeline_ids = cfg.get_pipelines(repo_id_int)
    pipelines = []
    for pipeline_id in pipeline_ids:
        pipelines.append({
            'id': pipeline_id,
            'ci_entrypoint': cfg.get_ci_entrypoint(pipeline_id),
            'name': cfg.get_pipeline_name(pipeline_id),
            'whitelist_user': ','.join(cfg.get_git_users_pipeline(pipeline_id)),
            'ci_branches': ",".join(cfg.get_branches_pipeline(pipeline_id)),
            'trigger_pr': cfg.get_trigger_pr(pipeline_id),
            })

    repo_url = cfg.get_repo_url(repo_id_int)
    repo_provider = 'unknown'
    if repo_url.startswith('https://github.com'):
        repo_provider = 'github'
        repo_webhook_url = f'{repo_url}/settings/hooks'
        repo_webhook_setup = f'Payload URL: {base_url}/webhook_ci?id={repo_id}'
        repo_webhook_setup += f'\nContent type: application/json'
        repo_webhook_setup += f'\nSecret: The webhook secret that you set in the global config below'
        repo_webhook_setup += f'\nWhich events would you like to trigger this webhook: Send me everything'
    if repo_url.startswith('https://bitbucket.org'):
        repo_provider = 'bitbucket'
        repo_webhook_url = f'{repo_url}/admin/webhooks'
        repo_webhook_setup = f'Title: Choose any name (e.g. CSCS-CI)'
        repo_webhook_setup += f'\nURL: {base_url}/webhook_ci?id={repo_id}'
        repo_webhook_setup += f'\nTriggers: Tick all boxes'
    if repo_url.startswith('https://gitlab.com'):
        repo_provider = 'gitlab'

    context = dict(
            repo_name             = cfg.get_repository_name(repo_id_int),
            repo_id               = repo_id,
            repo_url              = repo_url,
            is_private            = cfg.is_private_repo(repo_id_int),
            whitelist_user        = ",".join(cfg.get_git_users_default(repo_id_int)),
            ci_branches           = ",".join(cfg.get_branches_default(repo_id_int)),
            pipelines             = pipelines,
            selected_pipeline     = selected_pipeline,
            url_prefix            = request.app['url_prefix'],
            repo_provider         = repo_provider,
            repo_webhook_url      = repo_webhook_url,
            repo_webhook_setup    = repo_webhook_setup,
            ssh_key_public        = open(ssh_keyfile(repo_id_int, mirror=False, public=True)).read().strip(),
            favicon               = 'setup-favicon.svg'
            )

    response = aiohttp_jinja2.render_template("setup_ci.html", request, context=context)
    return response


@routes.post('/setup_ci')
async def handle_setup_ci_post(request:web.Request) -> web.Response:
    await log_request(request)
    if check_authorized(request) == False:
        return web.Response(status=401, headers={'WWW-Authenticate': 'Basic realm="Access to CI setup page"'})

    # we are authorized
    data = await request.post()
    username, password = parse_authorization(request)
    if username != data['repository_id']:
        mystr = f"={username}"
        return web.Response(status=403, text=f"Authorized for repo_id={username}, but altering repo_id={str(data['repository_id'])}. Mismatch.")

    # sanity check that all data are in fact strings
    assert isinstance(data['repository_id'], str)
    assert isinstance(data['name'], str)
    assert isinstance(data['notification_token'], str)
    assert isinstance(data['webhook_secret'], str)
    assert isinstance(data['whitelist_user'], str)
    assert isinstance(data['ci_branches'], str)

    repository_id = int(data['repository_id'])
    private_repo = 'privaterepo' in data
    private_changed = private_repo != cfg.is_private_repo(repository_id)
    cfg.update_private_repo(repository_id, private_repo)
    if data['notification_token']:
        cfg.update_notification_token(repository_id, data['notification_token'])
    if data['webhook_secret']:
        cfg.update_webhook_secret(repository_id, data['webhook_secret'])
    cfg.replace_git_users_default(repository_id, [x.strip() for x in data['whitelist_user'].split(',') if x])
    cfg.replace_branches_default(repository_id, [x.strip() for x in data['ci_branches'].split(',') if x])

    name_changed = cfg.get_repository_name(repository_id) != data['name']

    pipeline_ids = cfg.get_pipelines(repository_id)
    for pipeline_id in pipeline_ids:
        pipeline_name = data[f'{pipeline_id}_name']
        entrypoint = data[f'{pipeline_id}_entrypoint']
        whitelist_user = data[f'{pipeline_id}_whitelist_user']
        ci_branches = data[f'{pipeline_id}_ci_branches']
        trigger_pr = f'{pipeline_id}_trigger_pr' in data
        assert isinstance(pipeline_name, str)
        assert isinstance(entrypoint, str)
        assert isinstance(whitelist_user, str)
        assert isinstance(ci_branches, str)
        cfg.replace_branches_pipeline(pipeline_id, [x.strip() for x in ci_branches.split(',') if x])
        cfg.replace_git_users_pipeline(pipeline_id, [x.strip() for x in whitelist_user.split(',') if x])
        pipeline_name_changed = cfg.get_pipeline_name(pipeline_id) != pipeline_name
        entrypoint_changed = cfg.get_ci_entrypoint(pipeline_id) != entrypoint
        cfg.update_ci_entrypoint(pipeline_id, entrypoint)
        cfg.update_pipeline_name(pipeline_id, pipeline_name)
        cfg.update_trigger_pr(pipeline_id, trigger_pr)
        if pipeline_name_changed or name_changed or entrypoint_changed or private_changed:
            logger.info(f'pipeline_name_changed={pipeline_name_changed} name_changed={name_changed} entrypoint_changed={entrypoint_changed} private_changed={private_changed}')
            project = await get_mirror_repo(pipeline_id)
            if project is not None:
                project.name = f"{data['name']}-{pipeline_name}"
                project.ci_config_path = entrypoint
                project.visibility = 'private' if private_repo else 'public'
                project.save()

    # group visibility can only be changed after the projects visibility has been changed
    if name_changed or private_changed:
        logger.info(f'Group name changed from={cfg.get_repository_name(repository_id)} to={data["name"]}')
        cfg.update_repository_name(repository_id, data['name'])
        group = [x for x in glserver.groups.list(search=f'webhook-ci/mirrors/{repository_id}') if x.path==str(repository_id)]
        if group:
            group[0].name = data['name']
            group[0].visibility = 'private' if private_repo else 'public'
            group[0].save()


    location = f'/{url_prefix}{request.app.router["setup_ci"].url_for()}'
    logger.info(f'location redirection to location={location}, data={data}')
    raise web.HTTPFound(location=location)


# Deletes a pipeline by its ID.
# This will also delete the mirrored project in gitlab, thus all logfiles for this pipeline will not
# be accessible anymore
@routes.get('/pipeline_delete/{pipeline_id}')
async def pipeline_delete(request:web.Request) -> web.Response:
    await log_request(request)
    if check_authorized(request) == False:
        return web.Response(status=401, headers={'WWW-Authenticate': 'Basic realm="Access to CI setup page"'})

    pipeline_id = int(request.match_info['pipeline_id'])
    username, password = parse_authorization(request)
    if username != str(cfg.get_repository_id(pipeline_id)):
        return web.Response(status=403, text=f"Authorized for repo_id={username}, but deleting pipeline for repo_id={cfg.get_repository_id(pipeline_id)}. Mismatch.")

    # delete gitlab mirror repository
    mirror_repo = await get_mirror_repo(pipeline_id)
    if mirror_repo is not None:
        mirror_repo.delete()
    # delete public and private SSH keyfile
    ssh_keyfiles = (ssh_keyfile(pipeline_id, True, False), ssh_keyfile(pipeline_id, True, True))
    for f in ssh_keyfiles:
        if os.path.exists(f):
            os.remove(f)
    # cleanup database
    cfg.delete_pipeline(pipeline_id)

    location = f'/{url_prefix}{request.app.router["setup_ci"].url_for()}'
    raise web.HTTPFound(location=location)

# Crreate a new pipeline for a repository
# A repository can have many pipelines, but it has always at least one
# Each pipeline has its own name
@routes.get('/pipeline_create/{repository_id}')
async def pipeline_create(request: web.Request) -> web.Response:
    await log_request(request)
    if check_authorized(request) == False:
        return web.Response(status=401, headers={'WWW-Authenticate': 'Basic realm="Access to CI setup page"'})

    repository_id = int(request.match_info['repository_id'])
    username, password = parse_authorization(request)
    if username != request.match_info['repository_id']:
        return web.Response(status=403, text=f"Authorized for repo_id={username}, but creating pipeline for repo_id={repository_id}. Mismatch.")

    new_pipeline_id = cfg.new_pipeline(repository_id, 'unnamed')

    location = f'/{url_prefix}{request.app.router["setup_ci"].url_for()}?pipeline={new_pipeline_id}'
    raise web.HTTPFound(location=location)

@routes.get('/ci_run_no_pipeline_name')
async def ci_run_no_pipeline_name(request: web.Request) -> web.Response:
    await log_request(request)

    response = aiohttp_jinja2.render_template("errors/ci_run_no_pipeline_name.html", request, context={})
    return response



if __name__ == '__main__':
    app = web.Application()
    app.add_routes(routes)
    app['url_prefix'] = url_prefix
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(os.path.join(project_dir, 'templates')))
    logger.info(f'Running webserver {base_url}')
    web.run_app(app, port=port)
