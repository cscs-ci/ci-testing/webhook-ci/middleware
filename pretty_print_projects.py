import sqlite3
import sys

dbpath = 'middleware.db'

db = sqlite3.connect(dbpath)

search_term = f"where name LIKE '%{sys.argv[1]}%'" if len(sys.argv)>1 else ""
for repo_row in db.execute(f'select `repository_id`,`name`,`webhook_secret`,`repo_url`,`private`,`notification_token` from repository {search_term}'):
    default_branches = [ x[0] for x in db.execute('select branch from branch_default where repository_id=?', (repo_row[0],))]
    default_git_users = [ x[0] for x in db.execute('select username from git_user_default where repository_id=?', (repo_row[0],))]

    print(f'{repo_row[1]} (ID: {repo_row[0]} URL: {repo_row[3]})')
    print(f'https://cicd-ext-mw.cscs.ch/ci/setup_ci')
    print(f'\tprivate: {bool(repo_row[4])}')
    print(f'\twebhook_secret: {repo_row[2]}')
    print(f'\tnotification_token: {repo_row[5]}')
    print(f'\tDefault branches: {",".join(default_branches)}')
    print(f'\tDefault whitelisted: {",".join(default_git_users)}')

    for pipeline_row in db.execute('select `pipeline_id`,`repo_url`,`allow_craylic`,`ci_entrypoint`,`name` from pipeline where repository_id=?', (repo_row[0],)):
        pipeline_branches = [ x[0] for x in db.execute('select branch from branch where pipeline_id=?', (pipeline_row[0],))]
        pipeline_users = [ x[0] for x in db.execute('select username from git_user where pipeline_id=?', (pipeline_row[0],))]
        pipeline_branches_text = f'branches: {",".join(pipeline_branches)}' if pipeline_branches else ""
        pipeline_users_text = f'whitelisted: {",".join(pipeline_users)}' if pipeline_users else ""
        print(f'\tPipeline {pipeline_row[4]} {pipeline_users_text}{pipeline_branches_text} (Mirror: {pipeline_row[1]} entrypoint: {pipeline_row[3]}{" allow_craylic" if pipeline_row[2] else ""}')
