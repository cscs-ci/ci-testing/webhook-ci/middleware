import logging
import random
import secrets
import sqlite3

from typing import Any, cast, Iterable, List, Tuple

logger = logging.getLogger()

class Config(object):
    def __init__(self, db_path: str):
        self._db = sqlite3.connect(db_path)
        self._db.execute('PRAGMA foreign_keys=ON')

    def _get_attribute(self, table: str, primary_id: int, attribute_name: str) -> Any:
        result = self._db.execute(f'select {attribute_name} from {table} where {table}_id=?', (primary_id,)).fetchone()
        if not result:
            logger.warning(f'ID={primary_id} was not found in table={table}. Requested attribute={attribute_name}')
            return None
        return result[0]

    def _get_project_attribute(self, project_id: int, attribute_name: str) -> Any:
        return self._get_attribute('project', project_id, attribute_name)
    def _get_repository_attribute(self, repository_id: int, attribute_name: str) -> Any:
        return self._get_attribute('repository', repository_id, attribute_name)
    def _get_pipeline_attribute(self, pipeline_id: int, attribute_name: str) -> Any:
        return self._get_attribute('pipeline', pipeline_id, attribute_name)

    def _set_attribute(self, table: str, primary_id: int, attribute_name: str, attribute_value: Any) -> bool:
        res = self._db.execute(f'update {table} set {attribute_name}=? where {table}_id=?', (attribute_value, primary_id))
        self._db.commit()
        return res.rowcount == 1 # type: ignore

    def _set_project_attribute(self, project_id: int, attribute_name: str, attribute_value: Any) -> bool:
        return self._set_attribute('project', project_id, attribute_name, attribute_value)
    def _set_repository_attribute(self, repository_id: int, attribute_name: str, attribute_value: Any) -> bool:
        return self._set_attribute('repository', repository_id, attribute_name, attribute_value)
    def _set_pipeline_attribute(self, pipeline_id: int, attribute_name: str, attribute_value: Any) -> bool:
        return self._set_attribute('pipeline', pipeline_id, attribute_name, attribute_value)


    def get_random_id(self, table: str) -> int:
        new_id = random.randint(1, 2**53)
        while self._db.execute(f'select {table}_id from {table} where {table}_id=?', (new_id,)).fetchone() != None:
            new_id = random.randint(1, 2**53)
        return new_id

    def get_next_id(self, table: str, ref_id: int) -> int:
        while self._db.execute(f'select {table}_id from {table} where {table}_id=?', (ref_id+1,)).fetchone() != None:
            ref_id += 1
        return ref_id+1


    def get_repo_url(self, repository_id: int) -> str:
        return cast(str, self._get_repository_attribute(repository_id, 'repo_url'))
    def get_mirror_url(self, pipeline_id: int) -> str:
        return cast(str, self._get_pipeline_attribute(pipeline_id, 'repo_url'))
    def update_mirror_url(self, pipeline_id: int, new_mirror_url: str) -> bool:
        return self._set_pipeline_attribute(pipeline_id, 'repo_url', new_mirror_url)

    def get_all_pipelines(self) -> List[int]:
        return [ x[0] for x in self._db.execute('select pipeline_id from pipeline') ]


    def get_pipelines(self, repository_id: int) -> List[int]:
        all_pipeline_ids = [x[0] for x in self._db.execute(f'select pipeline_id from pipeline where repository_id=?', (repository_id,))]
        return all_pipeline_ids
    def delete_pipeline(self, pipeline_id: int) -> bool:
        res = self._db.execute('delete from pipeline where pipeline_id=?', (pipeline_id,))
        self._db.commit()
        return res.rowcount == 1 # type: ignore
    def new_pipeline(self, repository_id: int, name: str) -> int:
        all_rows = self._db.execute('select pipeline_id from pipeline where repository_id=? order by pipeline_id desc limit 1', (repository_id,)).fetchone()
        pipeline_id = self.get_next_id('pipeline', all_rows[0]) if all_rows else self.get_random_id('pipeline')
        mirror_webhook_secret = secrets.token_hex(16)
        self._db.execute('insert into pipeline (pipeline_id,repository_id,webhook_secret,name,ci_entrypoint) VALUES (?,?,?,?,"")', (pipeline_id, repository_id, mirror_webhook_secret, name))
        self._db.commit()
        return pipeline_id


    def get_notification_token(self, repository_id: int) -> str:
        return cast(str, self._get_repository_attribute(repository_id, 'notification_token'))
    def update_notification_token(self, repository_id: int, new_notification_token: str) -> bool:
        return self._set_repository_attribute(repository_id, 'notification_token', new_notification_token)

    def get_webhook_secret(self, repository_id: int) -> str:
        return cast(str, self._get_repository_attribute(repository_id, 'webhook_secret'))
    def update_webhook_secret(self, repository_id: int, new_webhook_secret: str) -> bool:
        return self._set_repository_attribute(repository_id, 'webhook_secret', new_webhook_secret)

    def get_mirror_webhook_secret(self, pipeline_id: int) -> str:
        return cast(str, self._get_pipeline_attribute(pipeline_id, 'webhook_secret'))


    def is_private_repo(self, repository_id: int) -> bool:
        return cast(bool, self._get_repository_attribute(repository_id, 'private'))
    def update_private_repo(self, repository_id: int, is_private: bool) -> bool:
        return self._set_repository_attribute(repository_id, 'private', is_private)

    def allow_cray_license(self, pipeline_id: int) -> bool:
        return cast(bool, self._get_pipeline_attribute(pipeline_id, 'allow_craylic'))

    def get_notification_url(self, repository_id: int) -> str:
        return cast(str, self._get_repository_attribute(repository_id, 'notification_url'))
    def update_notification_url(self, repository_id: int, new_url: str) -> bool:
        return self._set_repository_attribute(repository_id, 'notification_url', new_url)

    def get_ci_entrypoint(self, pipeline_id: int) -> str:
        return cast(str, self._get_pipeline_attribute(pipeline_id, 'ci_entrypoint'))
    def update_ci_entrypoint(self, pipeline_id: int, new_entrypoint: str) -> bool:
        return self._set_pipeline_attribute(pipeline_id, 'ci_entrypoint', new_entrypoint)

    def get_repository_name(self, repository_id: int) -> str:
        return cast(str, self._get_repository_attribute(repository_id, 'name'))
    def update_repository_name(self, repository_id: int, new_name: str) -> bool:
        return self._set_repository_attribute(repository_id, 'name', new_name)
    def get_pipeline_name(self, pipeline_id: int) -> str:
        return cast(str, self._get_pipeline_attribute(pipeline_id, 'name'))
    def update_pipeline_name(self, pipeline_id: int, new_name: str) -> bool:
        return self._set_pipeline_attribute(pipeline_id, 'name', new_name)

    def get_repository_id(self, pipeline_id: int) -> int:
        return cast(int, self._get_pipeline_attribute(pipeline_id, 'repository_id'))

    def get_trigger_pr(self, pipeline_id: int) -> bool:
        return cast(bool, self._get_pipeline_attribute(pipeline_id, 'trigger_pr'))
    def update_trigger_pr(self, pipeline_id: int, should_trigger: bool) -> bool:
        return self._set_pipeline_attribute(pipeline_id, 'trigger_pr', should_trigger)

    def get_branches_default(self, repository_id: int) -> List[str]:
        return [ x[0] for x in self._db.execute('select branch from branch_default where repository_id=?', (repository_id,)) ]
    def replace_branches_default(self, repository_id: int, new_branches: List[str]) -> None:
        self._db.execute('delete from branch_default where repository_id=?', (repository_id,))
        self._db.executemany('insert into branch_default (repository_id,branch) VALUES (?,?)', zip([repository_id]*len(new_branches), new_branches))
        self._db.commit()

    def get_branches_pipeline(self, pipeline_id: int) -> List[str]:
        return [ x[0] for x in self._db.execute('select branch from branch where pipeline_id=?', (pipeline_id,)) ]
    def replace_branches_pipeline(self, pipeline_id: int, new_branches: List[str]) -> None:
        self._db.execute('delete from branch where pipeline_id=?', (pipeline_id,))
        self._db.executemany('insert into branch (pipeline_id,branch) VALUES (?,?)', zip([pipeline_id]*len(new_branches), new_branches))
        self._db.commit()

    def get_git_users_default(self, repository_id: int) -> List[str]:
        return [ x[0] for x in self._db.execute('select username from git_user_default where repository_id=?', (repository_id,)) ]
    def replace_git_users_default(self, repository_id: int, new_users: List[str]) -> None:
        self._db.execute('delete from git_user_default where repository_id=?', (repository_id,))
        self._db.executemany('insert into git_user_default (repository_id,username) VALUES (?,?)', zip([repository_id]*len(new_users), new_users))
        self._db.commit()
    def get_git_users_pipeline(self, pipeline_id: int) -> List[str]:
        return [ x[0] for x in self._db.execute('select username from git_user where pipeline_id=?', (pipeline_id,)) ]
    def replace_git_users_pipeline(self, pipeline_id: int, new_users: List[str]) -> None:
        self._db.execute('delete from git_user where pipeline_id=?', (pipeline_id,))
        self._db.executemany('insert into git_user (pipeline_id,username) VALUES (?,?)', zip([pipeline_id]*len(new_users), new_users))
        self._db.commit()

    def add_status_update_needed(self, pipeline_id: int, target_url: str, sha: str, name: str) -> None:
        row = self._db.execute('select pipeline_id from status_update_needed where pipeline_id=? and sha=? and name=?', (pipeline_id, sha, name)).fetchone()
        if not row:
            self._db.execute('insert into status_update_needed (pipeline_id,target_url,sha,name) VALUES (?,?,?,?)', (pipeline_id, target_url,sha,name))
            self._db.commit()
    def del_status_update_needed(self, pipeline_id: int, target_url: str, sha: str, name: str) -> None:
        self._db.execute('delete from status_update_needed where pipeline_id=? and sha=? and name=?', (pipeline_id, sha, name))
        self._db.commit()
    def get_status_updates_needed(self) -> List[Tuple[int, str, str, str]]:
        return [x for x in self._db.execute('select pipeline_id,target_url, sha,name from status_update_needed')]
