CREATE TABLE projects (
    project_id INTEGER PRIMARY KEY,
    orig_url TEXT NOT NULL,
    mirror_url TEXT,
    notification_token TEXT,
    webhook_secret TEXT,
    mirror_webhook_secret TEXT,
    private BOOLEAN,
    allow_craylic BOOLEAN,
    notification_url TEXT,
    ci_entrypoint TEXT,
    name TEXT
);

CREATE TABLE branches (
    project_id INTEGER,
    branch TEXT
);

CREATE TABLE git_users (
    project_id INTEGER,
    username TEXT
);
