# this script runs a script on all projects mirrored to gitlab (manual intervention on changes, without deleting the project in gitlab)
import gitlab
import os
import sqlite3

from typing import Any


def ci_project_id_from_project(project: Any) -> int:
    return int(project.web_url.split('-')[-1])


dbpath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'middleware.db')
print(f'Using database {dbpath}')
con = sqlite3.connect(dbpath)
con.execute('PRAGMA foreign_keys=ON;')

glserver = gitlab.Gitlab.from_config() # type: ignore

all_project_ids = [x[0] for x in con.execute('select project_id from projects')]
all_project_urls = [x[0] for x in con.execute('select mirror_url from projects')]
all_projects = list(filter(lambda p: p.http_url_to_repo in all_project_urls, glserver.projects.list(membership=True, search_namespaces=True, search='ci-testing/webhook-ci')))

print(f'all_names={[x.name for x in all_projects]}')
for project in all_projects:
    project.emails_disabled = True
    project.save()

#print(f'hooks={[p.hooks.list()[0].url for p in all_projects]}')
#for project in all_projects:
#    hooks = project.hooks.list()
#    assert(len(hooks) == 1)
#    hook = hooks[0]
#    hook.url = hook.url.replace('http://148.187.148.110:8358', 'https://cicd-ext-mw.cscs.ch/citest')
#    hook.url = hook.url.replace('http://148.187.148.110:8357', 'https://cicd-ext-mw.cscs.ch/ci')
#    hook.save()

