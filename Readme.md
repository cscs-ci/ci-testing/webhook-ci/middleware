Required python packages: aiohttp, gitlab

Create file `$HOME/.python-gitlab.cfg` with content:
```
[global]
default = gitlabcom
ssl_verify = true
timeout = 5

[gitlabcom]
url = https://gitlab.com
private_token = MY_PRIVATE_TOKEN
api_version = 4
```
