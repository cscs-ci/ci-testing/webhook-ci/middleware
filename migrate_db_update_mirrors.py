# this script runs a script on all projects mirrored to gitlab (manual intervention on changes, without deleting the project in gitlab)
import base64
from dataclasses import dataclass
import gitlab
import os
import random
import requests
import sqlite3
import sys
import time

from typing import Any
from aiohttp import web, ClientSession


def ci_project_id_from_project(project: Any) -> int:
    return int(project.web_url.split('-')[-1])

@dataclass
class OldProject:
    project_id: int
    orig_url: str
    mirror_url: str
    notification_token: str
    webhook_secret: str
    mirror_webhook_secret: str
    private: bool
    allow_craylic: bool
    notification_url: str
    ci_entrypoint: str
    name: str

@dataclass
class OldBranches:
    project_id: int
    branch: str
@dataclass
class OldGitUsers:
    project_id: int
    username: str

def new_random_id(table: str, field: str) -> int:
    new_id = random.randint(1, 2**53)
    while con_new.execute(f'select {field} from {table} where {field}=?', (new_id,)).fetchone() != None:
        new_id = random.randint(1, 2**53)
    return new_id


dbpath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'middleware_old.db')
dbpath_new = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'middleware_new.db')
if os.path.exists(dbpath_new): os.remove(dbpath_new)
print(f'Using database {dbpath}')
con = sqlite3.connect(dbpath)
con_new = sqlite3.connect(dbpath_new)

all_projects_db_old = [ OldProject(project_id=x[0],
        orig_url=x[1],
        mirror_url=x[2],
        notification_token=x[3],
        webhook_secret=x[4],
        mirror_webhook_secret=x[5],
        private=x[6],
        allow_craylic=x[7],
        notification_url=x[8],
        ci_entrypoint=x[9],
        name=x[10]) for x in con.execute('select project_id,orig_url,mirror_url,notification_token,webhook_secret,mirror_webhook_secret,private,allow_craylic,notification_url,ci_entrypoint,name from projects') ]
all_git_users_old = [ OldGitUsers(project_id=x[0],
    username=x[1]) for x in con.execute('select project_id,username from git_users') ]
all_branches_old = [ OldBranches(project_id=x[0],
    branch=x[1]) for x in con.execute('select project_id,branch from branches') ]

con.execute('PRAGMA foreign_keys=ON;')
con_new.execute('PRAGMA foreign_keys=ON;')
con_new.execute('''CREATE TABLE project (
    project_id INTEGER PRIMARY KEY,
    admin TEXT,
    name TEXT)''')
con_new.execute('''CREATE TABLE repository (
    repository_id INTEGER PRIMARY KEY,
    project_id INTEGER,
    repo_url TEXT NOT NULL,
    notification_token TEXT,
    webhook_secret TEXT,
    private BOOLEAN,
    notification_url TEXT,
    name TEXT,
    FOREIGN KEY(project_id) REFERENCES project(project_id)
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)''')
con_new.execute('''CREATE TABLE pipeline (
    pipeline_id INTEGER PRIMARY KEY,
    repository_id INTEGER,
    repo_url TEXT,
    webhook_secret TEXT,
    allow_craylic BOOLEAN,
    ci_entrypoint TEXT,
    name TEXT,
    FOREIGN KEY(repository_id) REFERENCES repository(repository_id)
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)''')
con_new.execute('''CREATE TABLE branch_default (
    repository_id INTEGER,
    branch TEXT,
    FOREIGN KEY(repository_id) REFERENCES repository(repository_id)
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)''')
con_new.execute('''CREATE TABLE branch (
    pipeline_id INTEGER,
    branch TEXT,
    FOREIGN KEY(pipeline_id) REFERENCES pipeline(pipeline_id)
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)''')
con_new.execute('''CREATE TABLE git_user_default (
    repository_id INTEGER,
    username TEXT,
    FOREIGN KEY(repository_id) REFERENCES repository(repository_id)
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)''')
con_new.execute('''CREATE TABLE git_user (
    pipeline_id INTEGER,
    username TEXT,
    FOREIGN KEY(pipeline_id) REFERENCES pipeline(pipeline_id)
        ON UPDATE RESTRICT
        ON DELETE CASCADE
)''')

glserver = gitlab.Gitlab.from_config() # type: ignore
mirror_group = [x for x in glserver.groups.list(search='webhook-ci/mirrors') if x.name=='mirrors'][0]
webhook_group_projects = [x for x in glserver.groups.list(search='webhook-ci') if x.name=='webhook-ci'][0].projects.list()

for old_project in [ x for x in all_projects_db_old ]:
    print(f'Handling project={old_project}')
    cscs_project_id = new_random_id('project', 'project_id')
    if old_project.orig_url.endswith('.git'): old_project.orig_url = old_project.orig_url[:-4]
    con_new.execute(f'''insert into project (project_id,name) VALUES ({cscs_project_id}, '{old_project.name}')''')
    con_new.execute(f'''insert into repository (repository_id,project_id,repo_url,notification_token,webhook_secret,private,notification_url,name)
        VALUES (?,?,?,?,?,?,?,?)''', (old_project.project_id,cscs_project_id,old_project.orig_url,old_project.notification_token,
            old_project.webhook_secret,old_project.private,old_project.notification_url,old_project.name))
    pipeline_id = new_random_id('pipeline', 'pipeline_id')
    mirror_repo_url = f'https://gitlab.com/cscs-ci/ci-testing/webhook-ci/mirrors/{old_project.project_id}/{pipeline_id}'
    con_new.execute(f'''insert into pipeline (pipeline_id,repository_id,repo_url,webhook_secret,allow_craylic,ci_entrypoint,name) VALUES (?,?,?,?,?,?,?)''',
            (pipeline_id,old_project.project_id,mirror_repo_url,old_project.mirror_webhook_secret,
                old_project.allow_craylic,old_project.ci_entrypoint,'default'))
    for branch in [ x for x in all_branches_old if x.project_id==old_project.project_id ]:
        con_new.execute(f'''insert into branch_default (repository_id,branch) VALUES (?,?)''', (old_project.project_id, branch.branch))
    for user in [ x for x in all_git_users_old if x.project_id==old_project.project_id ]:
        con_new.execute(f'''insert into git_user_default (repository_id,username) VALUES (?,?)''', (old_project.project_id, user.username))

    # Create namespace, move project from old to new url
    repo_groups = [x for x in glserver.groups.list(search=f'webhook-ci/mirrors/{old_project.project_id}') if x.path==str(old_project.project_id)]
    assert(len(repo_groups)<=1)
    repo_group = (repo_groups[0] if len(repo_groups)==1
            else glserver.groups.create({'name': old_project.name,
                'path': old_project.project_id,
                'parent_id': mirror_group.id,
                'emails_disabled': True,
                'visibility': 'private' if old_project.private else 'public'}))

    success = False
    while not success:
        all_group_projects = repo_group.projects.list()
        if len(all_group_projects)==0:
            # Project has not been moved yet, move it now
            project = glserver.projects.get([x for x in webhook_group_projects if x.path.endswith(str(old_project.project_id))][0].id)
            try:
                time.sleep(5)
                project.transfer_project(repo_group.id)
                success = True
            except Exception as e:
                print(f'Could not transfer project e={e}')
        else:
            success = True

    project = glserver.projects.get(repo_group.projects.list()[0].id)
    for b in project.protectedbranches.list():
        b.delete()

    # update pipeline statuses at original repository
    if old_project.name not in set(['Webhook-CI Test project', 'SPH-EXA', 'QuICC', 'hpx-local', 'utopia_testci', 'utopia', 'MSolve POC', 'Korali', 'SPH-EXA_mini-app - jgphpc']):
        for p in project.pipelines.list():
            status = p.status
            sha = p.sha
            this_pipeline_id = p.id
            target_url = f'https://gitlab.com/cscs-ci/ci-testing/webhook-ci/mirrors/{old_project.project_id}/{pipeline_id}/-/pipelines/{this_pipeline_id}'
            if old_project.private:
                target_url = f'{os.path.dirname(project.hooks.list()[0].url)}/pipeline_results/{pipeline_id}/{project.id}/{this_pipeline_id}'
            url = old_project.notification_url.replace('{sha}', sha)
            if old_project.orig_url.find('github.com') != -1:
                if status == 'running':
                    status = 'pending'
                if status == 'failed':
                    status = 'failure'
                if status == 'canceled':
                    status = 'error'
                headers = {
                        'Authorization': f'Bearer {old_project.notification_token}'
                    }
                data = {
                        'state': status,
                        'target_url': target_url,
                        'context': 'cscs/default'
                    }
            elif old_project.orig_url.find('bitbucket.org') != -1:
                if status == 'running' or status == 'pending': status = 'INPROGRESS'
                elif status == 'success': status = 'SUCCESSFUL'
                elif status == 'failed': status = 'FAILED'
                elif status == 'canceled': status = 'STOPPED'
                headers = {
                        'Authorization': f'Basic {base64.b64encode(old_project.notification_token.encode("utf-8")).decode("utf-8")}'
                    }
                data = {
                        'state': status,
                        'url': target_url,
                        'description': 'CSCS pipeline',
                        'key': 'cscs/default'
                    }
            r = requests.post(url, headers=headers, json=data)
            try:
                r.raise_for_status()
            except Exception as e:
                print(f'exception notifying repo e={e}')
            respText = r.text

    assert(old_project.name)
    project.name = f'{old_project.name}-default'
    project.path = str(pipeline_id)
    project.emails_disabled = True
    project.description = f'Mirror of {old_project.orig_url}'
    project.save()

    # update hooks
    hooks = project.hooks.list()
    assert(len(hooks)==1)
    hook = hooks[0]
    hook.url = hook.url[:hook.url.find('?')] + f'?repository_id={old_project.project_id}&pipeline_id={pipeline_id}'
    hook.save()

    # create variables
    cscs_registry_var = [ var for var in project.variables.list() if var.key=='CSCS_REGISTRY_PATH' ]
    cscs_registry_val = f'art.cscs.ch/contbuild/testing/anfink/{old_project.project_id}/{pipeline_id}'
    if len(cscs_registry_var)==0:
        cscs_registry_var = project.variables.create({'key': 'CSCS_REGISTRY_PATH', 'value': cscs_registry_val})
    else:
        cscs_registry_var[0].value = cscs_registry_val
        cscs_registry_var[0].save()

con_new.commit()

sys.exit(1)


all_project_ids = [x[0] for x in con.execute('select project_id from projects')]
all_project_urls = [x[0] for x in con.execute('select mirror_url from projects')]
all_projects = list(filter(lambda p: p.http_url_to_repo in all_project_urls, glserver.projects.list(membership=True, search_namespaces=True, search='ci-testing/webhook-ci')))

print(f'all_names={[x.name for x in all_projects]}')
for project in all_projects:
    project.emails_disabled = True
    project.save()

#print(f'hooks={[p.hooks.list()[0].url for p in all_projects]}')
#for project in all_projects:
#    hooks = project.hooks.list()
#    assert(len(hooks) == 1)
#    hook = hooks[0]
#    hook.url = hook.url.replace('http://148.187.148.110:8358', 'https://cicd-ext-mw.cscs.ch/citest')
#    hook.url = hook.url.replace('http://148.187.148.110:8357', 'https://cicd-ext-mw.cscs.ch/ci')
#    hook.save()

